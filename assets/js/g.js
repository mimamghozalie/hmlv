const CountDown = (id, date) => {
  var countDownDate = new Date(date).getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {
    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById(id).innerHTML =
      hours + ':' + minutes + ':' + seconds + '';

    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById(id).innerHTML = 'EXPIRED';
    }
  }, 1000);
};

// data
/**
 *
 * @param {*} car_id
 * @param {*} data
 *
 * data {
 *      "car_id": "",
 *      "title": ""
 *      "bidding_expired": "", // 2020 12 28 13:01:20
 *      "img_url": ""
 *      "lokasi": ""
 *      "tipe_model": ""
 *      "kapasitas_mesin": ""
 *      "km": ""
 *      "tipe_bahan_bakar": ""
 * }
 */
const CreateCardList = data => {
  const html = `
    <div class="col-md-4">
                <div class="lvb-card" id="${data.car_id}">
                    <h3 class="lvb-time" id="${data.car_id}_time">01:00:00</h3>
                    <div class="lvb-img">
                        <img src="${data.img_url}" alt="">
                    </div>
                    <span class="lvb-id"><b>${data.car_id}</b></span>
                    <div class="row" style="margin:0">
                        <div class="col-md-6">
                            <div class="lvb-mtitle">
                                <h2>${data.title}</h2>
                            </div>
                            <div class="lvb-mprice">
                                <h2  id="${data.car_id}_mprice">Rp 0</h2>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <p>${data.tipe_model}</p>
                            <p>${data.kapasitas_mesin}</p>
                            <p>${data.km}</p>
                            <p>${data.tipe_bahan_bakar}</p>
                        </div>
                    </div>

                    <div class="lvb-lokasi">
                        <span >Lokasi Mobil : ${data.lokasi}</span>
                    </div>
                </div>
            </div>
    `;
  $('#live_bidding').prepend(html);
  CountDown(`${data.car_id}_time`, data.bidding_expired);
};
