$(function() {
    $(document).ready(function () {

        var $hamburger = $(".hamburger");
        $hamburger.on("click", function (e) {
            $hamburger.toggleClass("is-active");
            // Do something else, like open/close menu
        });

    /*    AOS.init({
            duration: 800,
        });
*/
       /* $(document).on('click', 'a[href^="#"]', function (event) {
            if(!$( this ).hasClass( "not-scroll" )) {
                event.preventDefault();
                if (typeof ($($.attr(this, 'href')).offset()) !== 'undefined') {
                    $('html, body').animate({
                        scrollTop: $($.attr(this, 'href')).offset().top
                    }, 500);
                }
            }
        });*/

        $('a[data-rel^=lightcase]').lightcase();

        $( ".closeButton" ).click(function() {
            $(this).parent().parent('.show-banner').hide();
        });
       /* $( "#closeButtonTop" ).click(function() {
            $('.show-banner.top').hide();
        });
*/
        $('.mobil-carousel').owlCarousel({
            loop:true,
            margin:10,
            dots:true,
            nav:false,
            items:1,
            autoplay:true,
            autoplayTimeout:4000
        });

    });
});