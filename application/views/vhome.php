<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html ng-app="digih" ng-controller="myCtrl"  lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

    <title>Hanggar Mobil</title>
</head>
<body>

<header class="wrap-header">
    <div class="show-banner top">
        <!--<div class="wrapper">-->
        <!--    <button type="button" class="closeButton">-->
        <!--        <img src="images/x.png" class="img-fluid img-close" />-->
        <!--    </button>-->
        <!--    <img src="images/footer-banner.jpg" class="img-fluid img-float" />-->
        <!--</div>-->
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand" href="#">
                <img src="<?php echo base_url();?>assets/images/logo.png" class="img-fluid" />
            </a>
            <button class="navbar-toggler hamburger hamburger--elastic" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMainContent" type="button" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
         
               <div class="collapse navbar-collapse" id="navbarMain">
                <div class="container">
                    <ul class="navbar-nav ml-auto justify-content-lg-end">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kerjakami">Cara Kerja Kami</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="<?php echo base_url();?>tentang">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>lokasi">Lokasi Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kontak">Kontak</a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    </div>

</header>

<main>
    <section class="section section-banner">
        <div class="container position-relative">
            <div class="row flex-md-row-reverse justify-content-end">
                <div class="col-md-7 col-lg-8">
                    <div class="row">
                        <div class="col-lg-7">
                            <h1>
                                Jual Mobil Anda Sekarang,
                                Tanpa Repot, Transparan dan Maksimal
                            </h1>
                            <p>Dapatkan nilai dan harga maksimal penjualan
                                mobil Anda melalui HanggarMobil.com.
                                Cek harga mobil Anda...
                                dan segera HANGGAR-in mobilmu
                                sekarang !!!</p>
                        </div>
                    </div>

                    <div class="d-none d-md-block">
                        <img src="<?php echo base_url();?>assets/images/img-banner.png" class="img-fluid img-banner" />
                    </div>
                </div>

                <div class="col-md-5 col-lg-4">
                    <form method='post' action='<?php echo base_url();?>inspeksi'>
                    <div class="form-group relative cz-select">
                           <select required ng-disabled="loadApi.merk" ng-change="find_model(pmerk);loadApi.model = true" ng-options="x.merk as x.merk for x in smerk" ng-model="pmerk"  class="form-control">
                                <option disabled value="">Pilih Merk</option>
                            </select>
                            <input type="hidden" name="pmerk" value="{{pmerk}}" >
                            <div class="loader" ng-if="!pmerk">
                                <i class="material-icons" ng-if="!loadApi.merk">keyboard_arrow_down</i>
                                <div class="loading-indicator" ng-if="loadApi.merk"></div>
                            </div>
                            <div class="loader" ng-if="pmerk"><i class="material-icons success">check</i></div>
                            
                        </div>

                        <!-- Pilih Model -->
                        <div class="form-group relative cz-select">
                            <select required ng-disabled="loadApi.model || loadApi.model == null" 
                                ng-change="find_tahun(pmerk,pmodel);loadApi.tahun = true" 
                                ng-options="x.model as x.model for x in smodel" 
                                ng-model="pmodel"  class="form-control" required>
                                    <option disabled value="">Pilih Model</option>
                            </select>
                            <input type="hidden" name="pmodel" value="{{pmodel}}" >
                            <div class="loader" ng-if="!pmodel" ng-switch="loadApi.model">
                                <i ng-switch-when="null" class="material-icons">keyboard_arrow_down</i>
                                <div ng-switch-when="true" class="loading-indicator"></div>
                                <i ng-switch-default class="material-icons">keyboard_arrow_down</i>
                            </div>
                            <div class="loader" ng-if="pmodel"><i class="material-icons success">check</i></div>
                        </div>

                        <!-- Pilih Tahun -->
                        <div class="form-group relative cz-select">
                            <select required ng-disabled="loadApi.tahun || loadApi.tahun == null" ng-change="find_tipemodel(pmerk,pmodel,ptahun);loadApi.tipe_model = true" ng-options="x.year as x.year for x in stahun" ng-model="ptahun"  class="form-control" required>
                                <option disabled value="">Pilih Tahun</option>
                            </select>
                            <input type="hidden" name="ptahun" value="{{ptahun}}" >
                            <div class="loader" ng-if="!ptahun" ng-switch="loadApi.tahun">
                                <i ng-switch-when="null" class="material-icons">keyboard_arrow_down</i>
                                <div ng-switch-when="true" class="loading-indicator"></div>
                                <i ng-switch-default class="material-icons">keyboard_arrow_down</i>
                            </div>
                            <div class="loader" ng-if="ptahun"><i class="material-icons success">check</i></div>
                        </div>

                        <!-- pilih Tipe Model -->
                        <div class="form-group relative cz-select">
                            <select required ng-disabled="loadApi.tipe_model || loadApi.tipe_model == null" 
                                ng-change="find_transmisi(pmerk,pmodel,ptahun,ptmodel);loadApi.transmisi = true" 
                                ng-options="x.trim as x.trim for x in stmodel" 
                                ng-model="ptmodel"  class="form-control" required>
                                
                                <option disabled value="">Tipe Model</option>
                            </select>
                            <input type="hidden" name="ptmodel" value="{{ptmodel}}" >
                            <div class="loader" ng-if="!ptmodel" ng-switch="loadApi.tipe_model">
                                <i ng-switch-when="null" class="material-icons">keyboard_arrow_down</i>
                                <div ng-switch-when="true" class="loading-indicator"></div>
                                <i ng-switch-default class="material-icons">keyboard_arrow_down</i>
                            </div>
                            <div class="loader" ng-if="ptmodel"><i class="material-icons success">check</i></div>
                        </div>

                        <!-- pilih Transmisi -->
                        <div class="form-group relative cz-select">
                             <select required ng-disabled="loadApi.transmisi || loadApi.transmisi == null"
                             ng-change="find_area();loadApi.area=true"
                             ng-options="x.transmission as x.transmission for x in stransmisi" 
                             ng-model="ptransmisi"  class="form-control" required>
                                <option disabled value="">Transmisi</option>
                            </select>
                            <input type="hidden" name="ptransmisi" value="{{ptransmisi}}" >
                            <div class="loader" ng-if="!ptransmisi" ng-switch="loadApi.transmisi">
                                <i ng-switch-when="null" class="material-icons">keyboard_arrow_down</i>
                                <div ng-switch-when="true" class="loading-indicator"></div>
                                <i ng-switch-default class="material-icons">keyboard_arrow_down</i>
                            </div>
                            <div class="loader" ng-if="ptransmisi"><i class="material-icons success">check</i></div>
                        </div>

                        <!-- Pilih Area -->
                        <div class="form-group relative cz-select">
                            <select required ng-disabled="loadApi.area || loadApi.area == null" ng-change="" ng-options="x.location as x.location for x in sarea" ng-model="parea"  class="form-control" required>
                                <option disabled value="">Area Inspeksi</option>
                            </select>
                            <input type="hidden" name="parea" value="{{parea}}" >
                            <div class="loader" ng-if="!parea" ng-switch="loadApi.area">
                                <i ng-switch-when="null" class="material-icons">keyboard_arrow_down</i>
                                <div ng-switch-when="true" class="loading-indicator"></div>
                                <i ng-switch-default class="material-icons">keyboard_arrow_down</i>
                            </div>
                            <div class="loader" ng-if="parea"><i class="material-icons success">check</i></div>
                        </div>

                        <div class="form-group relative">
                            <input required ng-model="pnama" type="text" class="form-control" placeholder="Nama">
                            <input type="hidden" name="pnama" value="{{pnama}}" >
                            <div class="loader" ng-if="pnama"><i class="material-icons success">check</i></div>
                        </div>
                        <div class="form-group relative">
                            <div class="loader" ng-if="checkEmailStatus"><i class="material-icons success">check</i></div>
                            <input required ng-model="pemail" ng-change="checkEmail()" type="email" class="form-control" placeholder="Email">
                            <input type="hidden" name="pemail" value="{{pemail}}" >
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">+62</div>
                            </div>
                            <input required ng-model="pnohp" ng-change="checkNoHp()" minlength="6" type="number" class="form-control" id="inlineFormInputGroup" placeholder="Nomor hape tanpa 0 di depan">
                            <div class="loader" ng-if="checkNoHpStatus">
                                <i class="material-icons success">check</i>
                            </div>
                        
                            <input type="hidden" name="pnohp" value="{{pnohp}}">
                        </div>
                        <!-- <div class="form-group">
                           <select ng-change="find_model(pmerk)" ng-options="x.merk as x.merk for x in smerk" ng-model="pmerk"  class="form-control" required>
                                <option disabled value="">Pilih Merk</option>
                            </select>
                            <input type="hidden" name="pmerk" value="{{pmerk}}" >
                        </div>
                        <div class="form-group">
                            <select ng-change="find_tahun(pmerk,pmodel)" ng-options="x.model as x.model for x in smodel" ng-model="pmodel"  class="form-control" required>
                                <option disabled value="">Pilih Model</option>
                            </select>
                            <input type="hidden" name="pmodel" value="{{pmodel}}" >
                        </div>
                        <div class="form-group">
                            <select ng-change="find_tipemodel(pmerk,pmodel,ptahun)" ng-options="x.year as x.year for x in stahun" ng-model="ptahun"  class="form-control" required>
                                <option disabled value="">Pilih Tahun</option>
                            </select>
                             <input type="hidden" name="ptahun" value="{{ptahun}}" >
                        </div>
                        <div class="form-group">
                            <select ng-change="find_transmisi(pmerk,pmodel,ptahun,ptmodel)" ng-options="x.trim as x.trim for x in stmodel" ng-model="ptmodel"  class="form-control" required>
                                <option disabled value="">Tipe Model</option>
                            </select>
                              <input type="hidden" name="ptmodel" value="{{ptmodel}}" >
                        </div>
                        <div class="form-group">
                             <select ng-change="find_area()" ng-options="x.transmission as x.transmission for x in stransmisi" ng-model="ptransmisi"  class="form-control" required>
                                <option disabled value="">Transmisi</option>
                            </select>
                              <input type="hidden" name="ptransmisi" value="{{ptransmisi}}" >
                        </div>
                        <div class="form-group">
                            <select ng-change="" ng-options="x.location as x.location for x in sarea" ng-model="parea"  class="form-control" required>
                                <option disabled value="">Area Inspeksi</option>
                            </select>
                             <input type="hidden" name="parea" value="{{parea}}" >
                        </div>

                        <div class="form-group">
                            <input ng-model="pnama" type="text" class="form-control" placeholder="Nama" required>
                             <input type="hidden" name="pnama" value="{{pnama}}" >
                        </div>
                        <div class="form-group">
                            <input ng-model="pemail" type="email" class="form-control" placeholder="Email" required>
                             <input type="hidden" name="pemail" value="{{pemail}}" >
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">+62</div>
                            </div>
                            <input ng-model="pnohp" type="number" class="form-control" id="inlineFormInputGroup" placeholder="Nomor hp tanpa 0 di depan" required>
                             <input type="hidden" name="pnohp" value="{{pnohp}}" >
                        </div> -->
                        <button type="submit" ng-disabled="!checkEmailStatus || !checkNoHpStatus" class="btn btn-green">CEK HARGA MOBIL</button>
                        <!-- <button type="submit" ng-disabled="!checkEmailStatus" class="btn btn-green">CEK HARGA MOBIL</button> -->
                    </form>
                    <div class="d-md-none">
                        <img src="<?php echo base_url();?>assets/images/img-banner.png" class="img-fluid img-banner" />
                    </div>
                </div>


            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-7 col-lg-8">
                    <h2>Kenapa harus di-<span>Hanggar?</span></h2>
                    <div class="row">
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="<?php echo base_url();?>assets/images/why-1.png" class="img-fluid mb-2" />
                            <h3>Terpercaya</h3>
                            <p class="small">Menjunjung tinggi kaidah<br/> penjualan yang sehat</p>
                        </div>
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="<?php echo base_url();?>assets/images/why-2.png" class="img-fluid mb-2" />
                            <h3>Transparansi</h3>
                            <p class="small">Proses lelang dapat <br/> dipantau bersama</p>
                        </div>
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="<?php echo base_url();?>assets/images/why-3.png" class="img-fluid mb-2" />
                            <h3>Mudah</h3>
                            <p class="small">Tidak pakai ribet <br/> dengan urusan perizinan</p>
                        </div>
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="<?php echo base_url();?>assets/images/why-4.png" class="img-fluid mb-2" />
                            <h3>Maksimal</h3>
                            <p class="small">Harga tender akan <br/> dicapai sampai maksimal</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-grey container">
        <div class="row align-items-center">
            <div class="col-md-5 col-lg-4">
                <div class="owl-carousel mobil-carousel">
                    <div class="item">
                        <img src="<?php echo base_url();?>assets/images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url();?>assets/images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url();?>assets/images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url();?>assets/images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url();?>assets/images/test.jpg" class="img-fluid" />
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-lg-8">
                <h2>Bagaimana cara <span>Hanggar</span>-in mobil?</h2>
                <div class="row align-items-start">
                    <div class="col-lg-5">
                        <img src="<?php echo base_url();?>assets/images/loop.png" class="img-fluid mb-2" />
                    </div>
                    <div class="col-lg-7 ">
                        <ol>
                            <li>
                                <strong>Cek kisaran harga mobil Anda serta booking waktu Anda untuk dating ke outlet kami.</strong>
                            </li>
                            <li>
                                <strong>Kami cek kondisi mobil Anda dan berdiskusi untuk dicapai harga awal dimulai lelang.</strong>
                            </li>
                            <li>
                                <strong>Melihat mobil Anda di lelang secara live, maksimal 30 menit per lelang.</strong>
                            </li>
                            <li>
                                <strong> Appointment langsung dilakukan oleh Anda dan pihak pemenang untuk jadwal pembayaran.</strong>
                            </li>
                        </ol>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class="show-banner">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="<?php echo base_url();?>assets/images/x.png" class="img-fluid img-close" />
            </button>
            <img src="<?php echo base_url();?>assets/images/footer-banner.jpg" class="img-fluid img-float" />
        </div>
    </div>


    <!--<div class="show-banner center">-->
    <!--    <div class="wrapper">-->
    <!--        <button type="button" class="closeButton">-->
    <!--            <img src="images/x.png" class="img-fluid img-close" />-->
    <!--        </button>-->
    <!--        <img src="images/test.jpg" class="img-fluid img-float" />-->
    <!--    </div>-->
    <!--</div>-->
</main>

<?php
    $this->load->view('vfooter');
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    var app = angular.module('digih', []);
    
    app.controller('myCtrl', function($scope,$http) {

    var url = 'http://localhost/hanggar_apis/';
    // var url = 'http://hanggarmobil.com/apis/';

    $scope.loadApi = {
        merk: true,
        model: null,
        tahun: null,
        tipe_model: null,
        transmisi: null,
        area: null
    };

    $scope.checkNoHpStatus = false;
    $scope.checkNoHp = () => {
        let i = $scope.pnohp.toString();

        if (i.length > 6) {
            $scope.checkNoHpStatus = true;
        } else {
            $scope.checkNoHpStatus = false;
        }
    }

    let validateEmail = (email) => {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $scope.checkEmailStatus = false;
    $scope.checkEmail = () => {
        let i = $scope.pemail;
        if (validateEmail(i)) {
            $scope.checkEmailStatus = true;
        } else {
            $scope.checkEmailStatus = false;

        }
    }

    $http.get(url + "merk_.php").success(function (result_DataA) { 
        $scope.smerk=[];           
        $scope.smerk =  result_DataA['data'];
        $scope.loadApi.merk = false;
     //   console.log( $scope.smerk); 

    });
    $scope.find_model = function(val){
        $scope.loadApi.model = true;
        $http.get(url + "model_.php",  {params:{"par": val}} ).success(function (result_DataA) { 
            $scope.smodel=[];           
            $scope.smodel =  result_DataA['data'];
            $scope.loadApi.model = false;
     //   console.log( $scope.smerk); 
        });
    }
    $scope.find_tahun = function(pmerk,pmodel){
        $http.get(url + "year_.php",  {params:{"pmerk": pmerk,"pmodel": pmodel}} ).success(function (result_DataA) { 
            $scope.stahun=[];           
            $scope.stahun =  result_DataA['data'];
            $scope.loadApi.tahun = false;
     //   console.log( $scope.smerk); 
        });
    }
    $scope.find_tipemodel = function(pmerk,pmodel,ptahun){
        $http.get(url + "tipemodel_.php",{params:{"pmerk": pmerk,"pmodel": pmodel,"ptahun": ptahun}} ).success(function (result_DataA) { 
        $scope.stmodel=[];           
        $scope.stmodel =  result_DataA['data'];
        $scope.loadApi.tipe_model = false;
     //   console.log( $scope.smerk); 
        });
    }
    $scope.find_transmisi = function(pmerk,pmodel,ptahun,ptmodel){
        $http.get(url + "transmisi_.php",{params:{"pmerk": pmerk,"pmodel": pmodel,"ptahun": ptahun,"ptmodel": ptmodel}} ).success(function (result_DataA) { 
            $scope.stransmisi=[];           
            $scope.stransmisi =  result_DataA['data'];
        //   console.log( $scope.smerk); 

            // console.log('5. Memuat Api Transmisi');
            $scope.loadApi.transmisi = false;
        });
    }
    $scope.find_area = function(){
        $http.get(url + "area_.php").success(function (result_DataA) { 
            $scope.sarea=[];           
            $scope.sarea =  result_DataA['data'];
        //   console.log( $scope.smerk); 

            // console.log('6. Memuat Api Area');
            $scope.loadApi.area = false;
        });
    }
     $scope.find_harga = function(pmerk,pmodel,ptahun,ptmodel,ptransmisi,parea,pnama,pemail,pnohp){
         if(!pmerk==''&&!pmodel==''&&!ptahun==''&&!ptmodel==''&&!ptransmisi==''&&!parea==''&&!pnama==''&&!pemail==''&&!pnohp==''){
        //       $http.get("http://hanggarmobil.com/apis/harga_.php",{params:{"pmerk": pmerk,"pmodel": pmodel,"ptahun": ptahun,"ptmodel": ptmodel,"ptransmisi": ptransmisi,"parea": parea}} ).success(function (result_DataA) { 
    //      var url = form.attributes["target"];
    // $log.debug(url);
  
    // $http
    //   .post(url, { email: $scope.email, name: $scope.name })
    //   .success(function (response) {
    //     $log.debug(response);
    //   })
    // }
        //     $scope.max_price =  result_DataA['data'][0]['max_price'];
        //     $scope.min_price =  result_DataA['data'][0]['min_price'];
        //   // console.log($scope.sarea); 
        //     // alert('min price: '+$scope.min_price +' max price: '+$scope.max_price)
        //     var x = $scope.max_price
        //     var n = $scope.min_price
        //     var xx = x.toString()
        //      var nn = n.toString()
        //     //console.log(xx + nn)
        //      window.location.href = "http://hanggarmobil.com/hanggardev/inspeksi.html?"+xx+"|"+nn+"|"+pmerk+"|"+pmodel+"|"+ptahun;
        //     });
        
            $http.post("http://hanggarmobil.com/hdev/inspeksi",{
                params:{"pmerk": "auh"}
                }).success(function (result_DataA) { 
        // $scope.mmail=[];           
        // $scope.mmail =  result_DataA;
     //   var pars = encodeURIComponent(pmerk)
   //window.location.href = "http://hanggarmobil.com/hdev/inspeksi?p="+pars
        });
            
         }else{
             alert('mohon cek kembali data Anda')
         }
       
    }
    
    });
        
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!--<script src="js/retina.min.js"></script>-->
<script src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lightcase.js"></script>
<script src="<?php echo base_url();?>assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script></body>
</html>