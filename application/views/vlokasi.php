<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html ng-app="digih" ng-controller="myCtrl"  lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

    <title>Hanggar Mobil</title>
</head>
<body>

<header class="wrap-header">
    <div class="show-banner top">
        <div class="wrapper">
            <!-- <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" /> -->
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand" href="#">
                <img src="<?php echo base_url();?>assets/images/logo.png" class="img-fluid" />
            </a>
            <button class="navbar-toggler hamburger hamburger--elastic" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMainContent" type="button" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
      <div class="collapse navbar-collapse" id="navbarMain">
                <div class="container">
                    <ul class="navbar-nav ml-auto justify-content-lg-end">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kerjakami">Cara Kerja Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>tentang">Tentang Kami</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url();?>lokasi">Lokasi Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kontak">Kontak</a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    </div>

</header>

<main>
    <section class="section section-banner" style="background-image:none;height: 300px;">
        <div class="container position-relative">
            <div class="row justify-content-end">
                <div class="col-md-7 col-lg-8">
                    <div class="row">
                        <div class="col-lg-8" style="text-align:center;">
                            <h1>
                                Lokasi Kami
                            </h1>
                            <p>Kami memfasilitasi penjual mobil bapak atau ibu dengan di bantu applikasi yang terpercaya serta tertransparant demi mendapatkan hasil yang maksimal</p>
                        </div>
                    </div>

                    <!-- <div class="d-none d-md-block">
                        <img src="images/img-banner.png" class="img-fluid img-banner" />
                    </div> -->
                </div>



            </div>
        </div>
    </section>

    

    <section class="bg-grey container">
          <div class="container position-relative">
            <div class="row justify-content-end">
                <div class="col-md-7 col-lg-8">
                    <div class="row">
                        <div class="col-lg-8">
                        <ol>
                            <li>
                                Cek kisaran harga mobil Anda lalu jadwalkan waktu Anda untuk datang ke outlet kami
                            </li>
                            <li>
                             
                                Kami bantu cek kondisi mobil Anda dan memberikan harga terbaik untuk dimulainya penawaran
                            </li>
                            <li>
                                Pantau penawaran mobil Anda secara real time pada saat itu juga
                            </li>
                            <li>
                                Atur jadwal pembayaran mobil Anda dengan penawar tertinggi
                            </li>
                        
                        </ol>

                    </div>
                    </div>

                    <!-- <div class="d-none d-md-block">
                        <img src="images/img-banner.png" class="img-fluid img-banner" />
                    </div> -->
                </div>



            </div>
        </div>
        <!--<div class="row align-items-center">-->
          
        <!--    <div class="col-md-7 col-lg-8">-->
               
        <!--        <div class="row align-items-start">-->
                   
        <!--            <div class="col-lg-7 ">-->
        <!--                <ol>-->
        <!--                    <li>-->
        <!--                        Cek kisaran harga mobil Anda lalu jadwalkan waktu Anda untuk datang ke outlet kami-->
        <!--                    </li>-->
        <!--                    <li>-->
                             
        <!--                        Kami bantu cek kondisi mobil Anda dan memberikan harga terbaik untuk dimulainya penawaran-->
        <!--                    </li>-->
        <!--                    <li>-->
        <!--                        Pantau penawaran mobil Anda secara real time pada saat itu juga-->
        <!--                    </li>-->
        <!--                    <li>-->
        <!--                        Atur jadwal pembayaran mobil Anda dengan penawar tertinggi-->
        <!--                    </li>-->
                        
        <!--                </ol>-->

        <!--            </div>-->

        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    </section>
    <!-- <div class="show-banner">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" />
        </div>
    </div> -->


    <!-- <div class="show-banner center">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/test.jpg" class="img-fluid img-float" />
        </div>
    </div> -->
</main>

<?php
    $this->load->view('vfooter');
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    var app = angular.module('digih', []);
    
    app.controller('myCtrl', function($scope,$http) {
       var v1 = parent.document.URL.substring(parent.document.URL.indexOf('?'), parent.document.URL.length);
       var v2 = v1.replace("?", "")  
      var ah = v2.split("|");
        var mmax = ah[0]
        var mmin = ah[1]
     
        
    $http.get("http://hanggarmobil.com/apis/merk_.php").success(function (result_DataA) { 
        $scope.smerk=[];           
        $scope.smerk =  result_DataA['data'];
     //   console.log( $scope.smerk); 

    });
    $scope.find_model = function(val){
        $http.get("http://hanggarmobil.com/apis/model_.php",  {params:{"par": val}} ).success(function (result_DataA) { 
        $scope.smodel=[];           
        $scope.smodel =  result_DataA['data'];
     //   console.log( $scope.smerk); 
        });
    }
   
    
    
    });
        
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!--<script src="js/retina.min.js"></script>-->
<script src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lightcase.js"></script>
<script src="<?php echo base_url();?>assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script></body>
</html>