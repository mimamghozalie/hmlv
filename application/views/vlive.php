<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html ng-app="digih" ng-controller="myCtrl"  lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

    <title>Hanggar Mobil - Live Bidding</title>
</head>
<body>

<header class="wrap-header">
    <div class="show-banner top">
        <div class="wrapper">
            <!-- <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" /> -->
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand" href="#">
                <img src="<?php echo base_url();?>assets/images/logo.png" class="img-fluid" />
            </a>
            <button class="navbar-toggler hamburger hamburger--elastic" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMainContent" type="button" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
        <div class="collapse navbar-collapse" id="navbarMain">
                <div class="container">
                    <ul class="navbar-nav ml-auto justify-content-lg-end">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kerjakami">Cara Kerja Kami</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="<?php echo base_url();?>tentang">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>lokasi">Lokasi Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kontak">Kontak</a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    </div>

</header>

<main>
    <section class="section section-banner" style="background-image:none;height: 200px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12" style="text-align:center;">
                <h1>
                    Live Bidding
                </h1>
                </div>
            </div>
        </div>
    </section>

    

    <section class="bg-grey container">
        <div class="row" id="live_bidding">
            <!-- <div class="col-md-4">
                <div class="lvb-card" id="ajkajk">
                    <h3 class="lvb-time" id="ajkajk_time">01:00:00</h3>
                    <div class="lvb-img">
                        <img src="<?php echo base_url()?>/assets/images/img-banner.png" alt="">
                    </div>
                    <span class="lvb-id"><b>AJKJKJ</b></span>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="lvb-mtitle">
                                <h2>Mazda 2 2010</h2>
                            </div>
                            <div class="lvb-mprice">
                                <h2  id="ajkajk_mprice">Rp 80.000.000</h2>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <p>1.5 Bensin</p>
                            <p>1498 CC</p>
                            <p>2000 KM</p>
                            <p>Bensin</p>
                        </div>
                    </div>

                    <div class="lvb-lokasi">
                        <span >Lokasi Mobil : Bintaro</span>
                    </div>
                </div>
            </div> -->

            <div id="no_live_bidding" style="text-align: center">
                <h2>Tidak ada live bidding.</h2>
            </div>
        </div>
    </section>
</main>

<?php
    $this->load->view('vfooter');
?>
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/socket.io.js"></script>
<script src="<?php echo base_url();?>assets/js/g.js"></script>

<!-- 
    ganti value harga mobil
    $('#ajkajk_mprice').text('Rp. '+ value)

    set timeout
    CountDown('id', '2020 01 28 10:00:00'), tag id & tanggal waktu habis
-->
<script>
    $(document).ready(function() {
        $.get( "http://localhost/hanggarmobil/assets/api/live_bidding_test.json", function( data ) {
            if (data.status == 'ok') {
                if (data.data.length > 0) {
                    $('#no_live_bidding').hide();
                    data.data.map(row => {
                        CreateCardList(row);
                    })
                } else {
                    $('#no_live_bidding').show();
                }
            }
        });
    });
//     CreateCardList({
//        "car_id": "mobil1",
//        "img_url": "http://localhost/hanggarmobil//assets/images/img-banner.png",
//        "title": "Pajero",
//        "lokasi": "sadsad",
//        "tipe_model": "sadd",
//        "bidding_expired": new Date().setHours(new Date().getHours() + 1),
//        "kapasitas_mesin": "sdsad",
//        "km": "sadsd",
//        "tipe_bahan_bakar": "sad",
//   })
 
    const socket = io('http://hm-socket.ghozalie.com');

    socket.on('update_price', msg => {
    console.log(msg.id_car);
    $(`#${msg.id_car}_mprice`).text('Rp ' + msg.price);
    });

    socket.on('update_list', data => {
        CreateCardList(data);
    });
</script>
</html>