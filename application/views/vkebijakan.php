<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html ng-app="digih" ng-controller="myCtrl"  lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

    <title>Hanggar Mobil</title>
</head>
<body>

<header class="wrap-header">
    <div class="show-banner top">
        <div class="wrapper">
            <!-- <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" /> -->
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand" href="#">
                <img src="<?php echo base_url();?>assets/images/logo.png" class="img-fluid" />
            </a>
            <button class="navbar-toggler hamburger hamburger--elastic" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMainContent" type="button" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
        <div class="collapse navbar-collapse" id="navbarMain">
                <div class="container">
                    <ul class="navbar-nav ml-auto justify-content-lg-end">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kerjakami">Cara Kerja Kami</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="<?php echo base_url();?>tentang">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>lokasi">Lokasi Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kontak">Kontak</a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    </div>

</header>

<main>
    <section class="section section-banner" style="background-image:none;height: 200px;">
        <div class="container position-relative">
            <div class="row justify-content-end">
                <div class="col-md-7 col-lg-8">
                    <div class="row">
                        <div class="col-lg-8" style="text-align:center;">
                            <h1>
                                Kebijakan Privasi
                            </h1>
                            <!--<p>Kami memfasilitasi penjual mobil bapak atau ibu dengan di bantu applikasi yang terpercaya serta tertransparant demi mendapatkan hasil yang maksimal</p>-->
                        </div>
                    </div>

                    <!-- <div class="d-none d-md-block">
                        <img src="images/img-banner.png" class="img-fluid img-banner" />
                    </div> -->
                </div>



            </div>
        </div>
    </section>

    

    <section class="bg-grey container">
          <div class="container position-relative">
            <div class="row left-content-end">
                <div class="col-md-7 col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                        <ol>
                            <li>
                               <strong>Komitmen kami terhadap privasi Anda</strong>
                               <br>
                                <p style="text-align: justify;margin-top:1rem;">
                                Hanggar Mobil berkomitmen untuk melindungi privasi pelanggan-pelanggannya di Indonesia. Kebijakan privasi berlaku untuk semua informasi pribadi yang dikumpulkan oleh kami atau dikirimkan kepada kami, baik offline atau online, termasuk informasi pribadi yang dikumpulkan atau dikirimkan melalui situs kami ("Situs Web") dan setiap situs mobile, aplikasi, widget, dan fitur interaktif mobile lainnya (secara kolektif, "Aplikasi" kami), melalui halaman jejaring sosial resmi kami yang kami kendalikan ("Halaman Jejaring Sosial" kami) serta melalui pesan email dengan format HTML yang kami kirimkan kepada Anda (secara kolektif, termasuk Halaman Jejaring Sosial, Aplikasi dan Situs Web, "Situs"). Apabila Anda tidak setuju dengan setiap ketentuan dalam Kebijakan Privasi ini, harap agar tidak menggunakan situs web kami atau mengirimkan informasi pribadi kepada kami.
                                </p>
                            </li>
                            <li>
                               <strong>Informasi pribadi</strong> 
                                <p style="text-align: justify;margin-top:1rem;">Informasi pribadi apa yang kami kumpulkan?<br>
                                Informasi Pribadi" adalah informasi yang mengidentifikasikan Anda sebagai seorang individu, seperti:<br>
                                
                                <ul class="kebijakan-ct-list">
                                •	<span>Nama</span> <br>
                                •	<span>Nomor telepon (termasuk nomor telepon rumah dan nomor telepon selular) </span> <br>
                                •	<span>Alamat email </span> <br>
                                •	<span>Gambar profil </span> <br>
                                •	<span>ID akun jejaring sosial </span> <br>
                                •	<span>Alamat IP </span> <br>
                                •	<span>informasi Penjelajah web (Browser) dan perangkat </span> <br>
                                </ul>
                                informasi server berkas Log<br>

                                <ul class="kebijakan-ct-list">
                                •	<span>Informasi yang dikumpulkan melalui cookie, tag pixel dan teknologi lainnya</span><br>
                                •	<span>Penggunaan data aplikasi</span><br>
                                •	<span>Informasi demografi dan informasi lainnya yang disediakan oleh Anda</span><br>
                                •	<span>Informasi lokasi</span><br>
                                •	<span>Informasi keseluruhan</span><br>
                                </ul>

                                Bagaimana kami mengumpulkan informasi pribadi?<br>
                                Kami dan penyedia layanan kami dapat mengumpulkan Informasi Pribadi dengan cara-cara yang beragam, termasuk:<br>
                                <ul class="kebijakan-ct-list">
                                •	<span>Melalui Situs: Kami dapat mengumpulkan Informasi Pribadi melalui Situs, misalnya, saat Anda checking harga atau booking.</span><br>
                                •	<span>Offline: Kami dapat mengumpulkan Informasi Pribadi dari Anda secara offline, seperti pada saat Anda menghubungi layanan pelanggan.</span><br>
                                •	<span>Dari Sumber Lainnya: Kami dapat menerima Informasi Pribadi dari sumber lainnya, seperti melalui database publik; mitra pemasaran bersama; platform jejaring sosial dari orang-orang dengan siapa Anda berteman atau dengan cara lain terhubung pada platform jejaring sosial, serta dari pihak ketiga lainnya. Sebagai contoh, jika Anda memilih untuk menghubungkan akun jejaring sosial Anda ke akun situs web Anda, Informasi Pribadi tertentu dari akun jejaring sosial Anda akan terbagi dengan kami, yang dapat mencakup Informasi Pribadi yang merupakan bagian dari profil Anda atau profil teman Anda.</span><br>
                                •	<span>Melalui browser atau perangkat: Informasi tertentu dikumpulkan oleh sebagian besar browser atau secara otomatis oleh perangkat Anda. Kami menggunakan informasi ini untuk tujuan statistikal dan juga untuk memastikan Situs kami berfungsi dengan baik.</span><br>
                                •	<span>Melalui server berkas log: "Alamat IP" Anda adalah nomor yang secara otomatis diberikan pada komputer atau perangkat Anda yang Anda gunakan dengan Penyedia Layanan Internet (ISP) Anda. Alamat IP diidentifikasikan dan secara otomatis dimasukan dalam server berkas log kami setiap saat pengguna mengunjungi Situs, bersama dengan waktu kunjungan dan halaman-halaman yang dikunjungi. Mohon dicatat bahwa kami memperlakukan Alamat IP, log file server dan informasi terkait sebagai Informasi Lainnya, kecuali yang mengharuskan kami untuk melakukan sebaliknya berdasarkan hukum yang berlaku.</span><br>
                                •	<span>Menggunakan cookies: Cookies memungkinkan server jaringan untuk mengalihkan data ke komputer atau perangkat untuk pencatatan dan tujuan lainnya. Apabila Anda tidak menginginkan informasi dikumpulkan melalui penggunaan cookies, terdapat prosedur singkat pada sebagian besar browser yang memungkinkan Anda untuk menolak penggunaan cookies. Untuk mempelajari lebih lanjut mengenai penggunaan cookies kami, Anda dapat melihat kebijakan cookies kami.</span><br>
                                </ul>

                                Melalui penggunaan Aplikasi oleh Anda. Saat Anda mengunduh dan menggunakan suatu Aplikasi, kami dan penyedia layanan dapat melacak dan mengumpulkan penggunaan data Aplikasi, seperti tanggal dan waktu ketika Aplikasi dalam perangkat Anda mengakses server kami dan informasi dan file apa yang telah diunduhkan ke Aplikasi berdasarkan nomor perangkat Anda.<br>
                                <ul class="kebijakan-ct-list">
                                # <span> Dari Anda: Informasi seperti tanggal lahir, gender dan kode pos, dan juga informasi lainnya, seperti sarana pilihan komunikasi Anda, dapat dikumpulkan pada saat Anda secara sukarela memberikan informasi ini. </span><br>
                                # <span>  Lokasi Fisik: Kami dapat mengumpulkan lokasi fisik atas perangkat Anda. </span><br>
                                </ul>
                                Dalam beberapa kasus, kami dapat menggunakan layanan pembayaran pihak ketiga untuk melakukan pembelian dan/atau mengumpulkan donasi melalui Situs. Dalam kasus-kasus tersebut, Informasi Pribadi Anda dapat dikumpulkan oleh pihak ketiga tersebut dan bukan oleh kami, dan hal tersebut merupakan subjek atas kebijakan privasi pihak ketiga, bukan daripada Kebijakan Privasi ini. Kita tidak memiliki kendali, dan tidak bertanggung jawab atas penggunaan atau pengungkapan oleh pihak ketiga atas Informasi Pribadi Anda.<br>
                                Bagaimana cara kami menggunakan Informasi Pribadi?<br>
                                Kami dapat menggunakan Informasi Pribadi:<br>

                                <ul class="kebijakan-ct-list">
                                • <span>	Untuk merespon pertanyaan-pertanyaan Anda dan memenuhi permintaan Anda, seperti untuk mengirimkan buletin atau untuk menjawab pertanyaan dan komentar Anda.</span><br>
                                • <span>	Untuk mengirimkan informasi administratif kepada Anda, sebagai contoh, informasi mengenai Situs dan perubahan ketentuan-ketentuan, syarat-syarat dan kebijakan-kebijakan kami. Karena informasi ini bisa saja penting untuk penggunaan Anda atas Situs, Anda tidak dapat memilih untuk tidak menerima komunikasi-komunikasi tersebut.</span><br>
                                • <span>	Untuk melengkapi dan memenuhi pembelian Anda dan/atau donasi Anda, sebagai contoh, untuk memproses pembayaran Anda, mengirimkan pesanan kepada Anda, berkomunikasi dengan Anda mengenai pembelian Anda dan menyediakan layanan pelanggan kepada Anda.</span><br>
                                • <span>	Untuk memberikan Anda pembaharuan dan pengumuman-pengumuman mengenai produk-produk, promosi dan pengumuman kami dan untuk mengirimkan Anda undangan untuk berpartisipasi dalam program spesial kami.</span><br>
                                • <span>	Untuk memungkinkan Anda untuk menghubungi dan dihubungi oleh pengguna lainnya melalui Situs, sebagaimana diijinkan oleh Situs yang berlaku.</span><br>
                                • <span>	Untuk mengijinkan Anda untuk berpartisipasi dalam papan pesan, obrolan, halaman profil dan blog dan layanan lainnya yang mana Anda dapat memasukan informasi dan materi (termasuk Halaman Jejaring Sosial kami).</span><br>
                                • <span>	Untuk tujuan usaha kami, seperti analisis dan pengelolaan usaha kami, penelitian pasar, audit, pengembangan produk baru, peningkatan Situs kami, peningkatan produk dan layanan kami, mengidentifikasikan tren penggunaan, menentukan efektifitas kampanye promosi kami, menyesuaikan pengalaman dan konten Situs berdasarkan kegiatan Anda sebelumnya pada Situs, dan mengukur kepuasan pelanggan dan menyediakan layanan pelanggan (termasuk pemecahan masalah sehubungan dengan masalah-masalah pelanggan).</span><br>
                                • <span>	Kami menggunakan informasi yang dikumpulkan melalui browser atau perangkat Anda untuk tujuan statistikal dan juga untuk memastikan bahwa Situs berfungsi dengan benar.</span><br>
                                • <span>	Untuk informasi detail tentang tujuan-tujuan yang mana kami menggunakan cookies, Anda dapat melihat kebijakan cookies kami.</span><br>
                                • <span>	Kami dapat menggunakan lokasi fisik perangkat Anda untuk memberikan Anda jasa dan konten yang telah di personalisasi berdasarkan lokasi. Kami dapat juga membagikan lokasi fisik perangkat Anda, dikombinasikan dengan informasi mengenai iklan-iklan apa saja yang telah Anda lihat dan informasi lainnya yang kami kumpulkan, dengan mitra pemasaran kami untuk dapat memungkinkan mereka untuk memberikan Anda dengan content yang lebih dipersonalisasi dan untuk mempelajari keefektifan kampanye periklanan.</span><br>
                                Sebagaimana yang kami anggap perlu atau pantas: 
                                    <ul class="kebijakan-ct-list">
                                        (a) <span> berdasarkan hukum yang berlaku, termasuk hukum-hukum di luar negara tempat tinggal; </span><br>
                                        (b) <span> untuk patuh dengan proses hukum; </span><br>
                                        (c) <span> untuk merespon permintaan dari otoritas publik dan pemerintah termasuk otoritas publik dan pemerintah di luar negara tempat tinggal; </span><br>
                                        (d) <span> untuk melaksanakan syarat dan ketentuan kami; </span><br>
                                        (e) <span> untuk melindungi kegiatan operasional kami atau salah satu afiliasi kami; </span><br>
                                        (f) <span> untuk melindungi hak-hak, privasi, keselamatan atau properti, dan/atau dari afiliasi kami, Anda atau lainnya; </span><br>
                                        (g) <span> untuk memungkinkan kami untuk mengejar upaya hukum yang tersedia atau membatasi kerusakan yang dapat kami pertahankan. </span><br>
                                    </ul>
                                </ul>
                                </p>
                            </li>
                            <li>
                               <strong>Keamanan</strong> 
                                <p class="kebijakan-ct">Kami menggunakan langkah-langkah organisasional, teknis dan administratif yang layak untuk melindungi Informasi Pribadi di bawah kendali kami. Sayangnya, tidak ada transmisi data melalui internet atau sistem penyimpanan data yang keamanannya terjamin 100%.<br>
                                Jika Anda memiliki alasan untuk percaya bahwa interaksi Anda dengan kami tidak lagi aman (sebagai contoh, jika Anda merasa bahwa keamanan akun yang Anda miliki terganggu), mohon agar segara memberitahukan masalah tersebut kepada kami dengan menghubungi kami sesuai dengan formulir bagian "Hubungi Kami".

</p>
                            </li>
                        <li>
                               <strong>Akses, modifikasi dan pilihan</strong> 
                                <p class="kebijakan-ct">Kami memberikan Anda banyak pilihan mengenai penggunaan dan pengungkapan oleh kami atas Informasi Pribadi Anda untuk tujuan pemasaran.
<br>
                               •	Apabila suatu saat Anda ingin berhenti untuk menerima komunikasi pemasaran dari kami, cara termudah yang dapat Anda lakukan adalah dengan menggunakan fitur unsubscribe dalam komunikasi pemasaran yang Anda terima. Anda juga dapat memberitahukan kami dengan mengirimkan email, telepon atau menuliskan kepada kami dengan menggunakan informasi kontak yang tercantum di bawah ini pada bagian "Hubungi Kami". Dalam permintaan Anda, mohon untuk menunjukan bahwa Anda ingin berhenti menerima komunikasi pemasaran dari kami.<br>
•	Selain itu, kami tidak akan mengungkapkan Informasi Pribadi Anda kepada pihak ketiga, termasuk anak perusahaan kami, untuk tujuan pemasaran langsung oleh pihak ketiga apabila kami telah menerima dan memproses permintaan dari Anda bahwa Informasi Pribadi Anda tidak akan dibagikan kepada pihak ketiga untuk tujuan tersebut. Jika Anda ingin mengirimkan permintaan tersebut, silahkan memberitahukan kami dengan menggunakan formulir "Hubungi Kami". Mohon untuk secara jelas mengindikasikan permintaan Anda bahwa Anda tidak ingin agar Informasi Pribadi Anda diungkapkan kepada anak perusahaan kami dan/atau pihak ketiga lainnya untuk tujuan pemasaran langsung.<br>
•	Perlu diketahui bahwa perubahan mungkin tidak akan efektif dengan segera. Kami akan berusaha untuk memenuhi permintaan Anda dengan segera sebagaimana dapat dilakukan secara wajar dan tidak lebih dari 30 hari setelah kami mendapatkan permintaan Anda. Perlu diketahui juga bahwa apabila Anda memilih untuk tidak menerima pesan terkait pemasaran dari kami, kami dapat tetap mengirimkan pesan administratif yang bersifat penting kepada Anda, dan Anda tidak dapat untuk tidak memilih untuk menerima pesan administratif tersebut.<br>
•	Untuk meninjau, mengkoreksi, membaharui, merahasiakan, menghapus atau membatasi penggunaan Informasi Pribadi Anda oleh kami yang sebelumnya telah diberikan kepada kami, silahkan gunakan formulir "Hubungi Kami" dan mohon dengan jelas untuk menerangkan permintaan Anda. Dalam permintaan Anda, mohon untuk dijelaskan informasi apa yang ingin Anda ubah, dirahasiakan dari database kami atau dibiarkan untuk diketahui oleh kami dengan batasan apa yang ingin Anda berikan atas penggunaan Informasi Pribadi Anda.<br>
•	Kami akan berusaha untuk memenuhi permintaan Anda sesegera mungkin. Mohon untuk diperhatikan bahwa meskipun kami telah berupaya, bisa saja ada sisa informasi dan catatan lainnya yang akan tetap berada dalam database kami, yang tidak akan dihapus ataupun diubah. Selanjutnya, perlu diketahui bahwa kami mungkin perlu untuk mempertahankan informasi tertentu untuk tujuan pencatatan dan/atau untuk penyelesaian transaksi yang telah Anda mulai sebelum permintaan atas perubahan atau penghapusan (misalnya, ketika Anda melakukan pembelian atau mengikuti suatu promosi, Anda bisa saja tidak dapat mengubah atau menghapus Informasi Pribadi yang diberikan sampai setelah selesainya pembelian atau promosi tersebut).<br>


</p>
                            </li>
                            <li>
                               <strong>Situs pihak ketiga</strong> 
                                <p class="kebijakan-ct">Kebijakan Privasi ini tidak membahas, dan kami tidak bertanggung jawab atas, privasi, informasi atau praktek-praktek lainnya dari pihak ketiga, termasuk pihak ketiga yang beroperasi pada situs apapun yang mana pada Situs Hanggar Mobil terdapat link atas situs tersebut. Dimasukannya link tersebut pada Situs Hanggar Mobil tidak berarti menunjukan dukungan atas situs terkait oleh kami dan afiliasi kami.<br>
Selain itu, kami dapat memberikan Anda akses kepada pihak ketiga yang memungkinkan Anda untuk mengirim konten ke akun jejaring sosial. Mohon dicatat bahwa setiap informasi yang Anda berikan melalui penggunaan fungsi ini diatur oleh kebijakan privasi pihak ketiga yang berlaku, dan bukan oleh Kebijakan Privasi ini. Kami tidak memiliki kendali, dan tidak bertanggung jawab atas, penggunaan pihak ketiga manapun atas informasi yang Anda berikan melalui penggunaan fungsi ini.<br>
Kami juga tidak bertanggung jawab atas pengumpulan, penggunaan dan pengungkapan kebijakan dan praktek (termasuk praktek keamanan data) dari organisasi lain, seperti Facebook, Apple, Google, Microsoft, RIM atau pengembang aplikasi, penyedia platform jejaring sosial, penyedia sistem operasi, penyedia layanan nirkabel atau pabrikan perangkat lainnya, termasuk Informasi Pribadi yang Anda ungkapkan kepada organisasi lainnya melalui atau sehubungan dengan Aplikasi atau Halaman Jejaring Sosial kami.


</p>
                            </li>
                            
                             <li>
                               <strong>Periode retensi</strong> 
                                <p class="kebijakan-ct">Kami akan mempertahankan Informasi Pribadi Anda untuk periode yang diperlukan untuk memenuhi tujuan yang diatur dalam Kebijakan Privasi ini dan sesuai dengan peraturan perundang-undangan yang berlaku.
</p>
                            </li>
                            
                             <li>
                               <strong>Informasi sensitif</strong> 
                                <p class="kebijakan-ct">Kami secara umum meminta Anda untuk tidak mengirimkan kepada kami, dan agar Anda tidak mengungkapkan, Informasi Pribadi yang sensitif (misalnya, informasi yang terkait dengan asal-usul ras atau etnis, pandangan politik, agama atau kepercayaan lainnya, kesehatan atau kondisi medis, latar belakang kriminal atau keanggotaan serikat buruh) pada atau melalui Situs atau kepada kami. Dalam pengecualian tertentu, seperti permintaan asuransi, kami akan menjelaskan kepada Anda bagaimana Anda dapat melakukan hal ini secara aman.

</p>
                            </li>
                            
                                <li>
                               <strong>Pembaruan atas kebijakan privasi ini</strong> 
                                <p class="kebijakan-ct">Kami dapat mengubah Kebijakan Privasi ini. Mohon untuk dilihat tulisan "Update Terakhir" pada bagian atas halaman ini untuk melihat kapan terakhir Kebijakan Privasi ini terakhir direvisi. Setiap perubahan atas Kebijakan Privasi ini akan menjadi efektif saat kami memasukan revisi Kebijakan Privasi ini pada Situs.


</p>
                            </li>
                            
                            <li>
                               <strong>Hubungi kami</strong> 
                                <p class="kebijakan-ct">Apabila Anda memiliki pertanyaan mengenai Kebijakan Privasi ini, mohon untuk menghubungi kami dengan klik disini.



</p>
                            </li>
                            
                        </ol>

                    </div>
                    </div>

                    <!-- <div class="d-none d-md-block">
                        <img src="images/img-banner.png" class="img-fluid img-banner" />
                    </div> -->
                </div>



            </div>
        </div>
        <!--<div class="row align-items-center">-->
          
        <!--    <div class="col-md-7 col-lg-8">-->
               
        <!--        <div class="row align-items-start">-->
                   
        <!--            <div class="col-lg-7 ">-->
        <!--                <ol>-->
        <!--                    <li>-->
        <!--                        Cek kisaran harga mobil Anda lalu jadwalkan waktu Anda untuk datang ke outlet kami-->
        <!--                    </li>-->
        <!--                    <li>-->
                             
        <!--                        Kami bantu cek kondisi mobil Anda dan memberikan harga terbaik untuk dimulainya penawaran-->
        <!--                    </li>-->
        <!--                    <li>-->
        <!--                        Pantau penawaran mobil Anda secara real time pada saat itu juga-->
        <!--                    </li>-->
        <!--                    <li>-->
        <!--                        Atur jadwal pembayaran mobil Anda dengan penawar tertinggi-->
        <!--                    </li>-->
                        
        <!--                </ol>-->

        <!--            </div>-->

        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    </section>
    <!-- <div class="show-banner">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" />
        </div>
    </div> -->


    <!-- <div class="show-banner center">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/test.jpg" class="img-fluid img-float" />
        </div>
    </div> -->
</main>

<?php
    $this->load->view('vfooter');
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    var app = angular.module('digih', []);
    
    app.controller('myCtrl', function($scope,$http) {
       var v1 = parent.document.URL.substring(parent.document.URL.indexOf('?'), parent.document.URL.length);
       var v2 = v1.replace("?", "")  
      var ah = v2.split("|");
        var mmax = ah[0]
        var mmin = ah[1]
     
        
    $http.get("http://hanggarmobil.com/apis/merk_.php").success(function (result_DataA) { 
        $scope.smerk=[];           
        $scope.smerk =  result_DataA['data'];
     //   console.log( $scope.smerk); 

    });
    $scope.find_model = function(val){
        $http.get("http://hanggarmobil.com/apis/model_.php",  {params:{"par": val}} ).success(function (result_DataA) { 
        $scope.smodel=[];           
        $scope.smodel =  result_DataA['data'];
     //   console.log( $scope.smerk); 
        });
    }
   
    
    
    });
        
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!--<script src="js/retina.min.js"></script>-->
<script src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lightcase.js"></script>
<script src="<?php echo base_url();?>assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script></body>
</html>