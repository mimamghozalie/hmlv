<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html ng-app="digih" ng-controller="myCtrl"  lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

    <title>Hanggar Mobil</title>
</head>
<body>

<header class="wrap-header">
    <div class="show-banner top">
        <div class="wrapper">
            <!-- <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" /> -->
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand" href="#">
                <img src="<?php echo base_url();?>assets/images/logo.png" class="img-fluid" />
            </a>
            <button class="navbar-toggler hamburger hamburger--elastic" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMainContent" type="button" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
        <div class="collapse navbar-collapse" id="navbarMain">
                <div class="container">
                    <ul class="navbar-nav ml-auto justify-content-lg-end">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kerjakami">Cara Kerja Kami</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="<?php echo base_url();?>tentang">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>lokasi">Lokasi Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kontak">Kontak</a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    </div>

</header>

<main>
    <section class="section section-banner" style="background-image:none;height: 500px;">
        <div class="container position-relative">
            <div class="row justify-content-end">
                <div class="col-md-7 col-lg-8">
                    <div class="row">
                        <div class="col-lg-8" style="text-align:center;">
                            <h1>
                                Syarat dan Ketentuan
                            </h1>
                            <p style="margin-bottom:0px;">Selamat datang di www.hanggarmobil.com</p>
                             <p style="text-align:justify;margin-bottom:0px;">Disarankan sebelum mengakses Situs ini lebih jauh, Anda terlebih dahulu membaca dan memahami syarat dan ketentuan yang berlaku. Syarat dan ketentuan berikut adalah ketentuan dalam pengunjungan Situs, isi dan/atau konten, layanan, serta fitur lainnya yang ada di www.hanggarmobil.com. Dengan mengakses atau menggunakan Situs www.hanggarmobil.com, informasi, atau aplikasi lainnya dalam bentuk mobile application yang disediakan oleh atau dalam Situs, berarti Anda telah memahami dan menyetujui serta terikat dan tunduk dengan segala syarat dan ketentuan yang berlaku di Situs ini.</p>
                        </div>
                    </div>

                    <!-- <div class="d-none d-md-block">
                        <img src="images/img-banner.png" class="img-fluid img-banner" />
                    </div> -->
                </div>



            </div>
        </div>
    </section>

    

    <section class="bg-grey container">
          <div class="container position-relative">
            <div class="row left-content-end">
                <div class="col-md-7 col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                        <ol>
                            <li>
                               <strong>Definisi</strong> 
                                <p class="kebijakan-ct">Setiap kata atau isitilah berikut yang digunakan di dalam Syarat dan Ketentuan ini memiliki arti seperti berikut di bawah, kecuali jika kata atau istilah yang bersangkutan di dalam pemakaiannya dengan tegas menentukan lain.<br>
                                    <ul class="kebijakan-ct-list">
                                        1.1. <span>“Kami”, berarti PT. Artha Hutama Andalan selaku pemilik dan pengelola Situs www.hanggarmobil.com, serta aplikasi lainnya dan/atau mobile application.</span><br>
                                        1.2. <span> “Anda”, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami.</span><br>
                                        1.3. <span> “Layanan”, berarti tiap dan keseluruhan jasa serta informasi yang ada pada Situs www.hanggarmobil.com, dan tidak terbatas pada informasi yang disediakan, layanan aplikasi dan fitur, dukungan data, serta mobile application yang disediakan oleh Kami.</span><br>
                                        1.4. <span> “Pengguna”, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami, termasuk diantaranya Pengguna Belum Terdaftar dan Pengguna Terdaftar.</span><br>
                                        1.5. <span> “Pengguna Terdaftar”, berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami, serta telah melakukan registrasi dan memiliki akun pada Situs Kami.</span><br>
                                        1.6. <span> “Pihak Ketiga”, berarti pihak ketiga manapun, termasuk namun tidak terbatas juga untuk menghindari keraguan, baik individu maupun entitas, pihak lain dalam kontrak, pemerintah, atau swasta.</span><br>
                                        1.7. <span> “Profil”, berarti data pribadi yang digunakan oleh Pengguna, dan menjadi informasi dasar bagi Pengguna.</span><br>
                                        1.8. <span> “Informasi Pribadi”, berarti tiap dan seluruh data pribadi yang diberikan oleh Pengguna di Situs Kami, termasuk namun tidak terbatas pada nama, nomor identifikasi, bukti kepemilikan, NPWP, surat izin usaha, lokasi pengguna, kontak pengguna, serta dokumen dan data lainnya sebagaimana diminta pada ringkasan pendaftaran akun serta pada ringkasan aplikasi pengajuan.</span><br>
                                        1.9. <span> “Konten”, berarti teks, data, informasi, angka, gambar, grafik, foto, audio, video, nama pengguna, informasi, aplikasi, tautan, komentar, peringkat, desain, atau materi lainnya yang ditampilkan pada Situs.</span><br>
                                    </ul>
                                </p>
                            </li>
                           
               
                            <li>
                               <strong>Layanan dan/atau jasa</strong> 
                                <p class="kebijakan-ct">Kami memfasilitasi menjual mobil Bapak atau Ibu dengan di bantu applikasi yang terpercaya serta terbuka demi mendapatkan hasil yang maksimal.<br>
                                <ul class="kebijakan-ct-list">
                                    1. <span>	Cek kisaran harga mobil Anda lalu jadwalkan waktu Anda untuk datang ke outlet kami.</span><br>
                                    2. <span>	Kami bantu cek kondisi mobil Anda dan memberikan harga terbaik untuk dimulainya penawaran.</span><br>
                                    3. <span>	Pantau penawaran mobil Anda secara real time pada saat itu juga.</span><br> 
                                    4. <span>	Atur jadwal pembayaran mobil Anda dengan penawar tertinggi. </span><br>
                                </ul>
                                </p>
                            </li>
                                                                
                            <li>
                               <strong>Hak intelektual properti</strong> 
                                <p class="kebijakan-ct">Semua Hak Kekayaan Intelektual yang ada di dalam Situs ini adalah kepunyaan dari Kami. Tiap atau keseluruhan informasi dan materi dan konten, termasuk tetapi tidak terbatas pada, tulisan, perangkat lunak, teks, data, grafik, gambar, audio, video, logo, ikon atau kode-kode html dan kode-kode lain yang ada di Situs ini dilarang dipublikasikan, dimodifikasi, disalin, digandakan atau diubah dengan cara apapun di luar area Situs ini tanpa izin dari Kami. Pelanggaran terhadap hak-hak Situs ini dapat ditindak sesuai dengan peraturan yang berlaku.</p>
                                    
                                Anda dapat menggunakan informasi atau isi dalam Situs hanya untuk penggunaan pribadi non-komersil. Kecuali ditentukan sebaliknya dan/atau diperbolehkan secara tegas oleh undang-undang hak cipta, maka Pengguna dilarang untuk menyalin, membagikan ulang, mentransmisi ulang, mempublikasi atau melakukan tindakan eksploitasi komersial dari pengunduhan yang dilakukan tanpa seizin pemilik Hak Intelektual Properti tersebut. Dalam hal Pengguna telah mendapatkan izin yang diperlukan maka Pengguna dilarang melakukan perubahan atau penghapusan. Pengguna dengan ini menyatakan menerima dan mengetahui bahwa dengan mengunduh materi Hak Intelektual Properti bukan berarti mendapatkan hak kepemilikan atas pengunduhan materi Hak Intelektual Properti tersebut.

                            </li>
                                                                
                            <li>
                                <strong>Komentar</strong> 
                                <p class="kebijakan-ct">Jika Anda ingin memberikan komentar, masukan, ataupun sanggahan mengenai tiap atau keseluruhan konten dan informasi dalam Situs, Kami juga menyediakan sarana chat/messaging, kontak email, blog, ataupun media sosial milik Kami. Anda diperbolehkan untuk memberi komentar, dan setuju bahwa jika diperlukan, akan Kami pergunakan untuk kepentingan informasi dan ditampilkan di Situs Kami. Dan harap diperhatikan bahwa komentar, masukan, atau sanggahan yang Anda berikan tidak boleh bertentangan dengan Syarat dan Ketentuan ini.

                                </p>
                            </li>
                                                                
                            <li>
                                <strong>Umum</strong> 
                                <p class="kebijakan-ct">
                                    <ul class="kebijakan-ct-list">
                                        5.1. <span> Penggunaan dan akses ke Situs ini diatur oleh Syarat dan Ketentuan serta Kebijakan Privasi Kami. Dengan mengakses atau menggunakan Situs ini, informasi, atau aplikasi lainnya dalam bentuk mobile application yang disediakan oleh atau dalam Situs, berarti Anda telah memahami dan menyetujui serta terikat dan tunduk dengan segala syarat dan ketentuan yang berlaku di Situs ini.</span><br>
                                        5.2. <span> Kami berhak untuk menutup atau mengubah atau memperbaharui Syarat dan Ketentuan ini setiap saat tanpa pemberitahuan, dan berhak untuk membuat keputusan akhir jika tidak ada ketidakcocokan. Kami tidak bertanggung jawab atas kerugian dalam bentuk apa pun yang timbul akibat perubahan pada Syarat dan Ketentuan.</span><br>
                                        5.3. <span> Jika Anda masih memerlukan jawaban atas pertanyaan yang tidak terdapat dalam Syarat dan Ketentuan ini, Anda dapat menghubungi Kami di email cs@hanggarmobil.com atau menghubungi kami di nomor 021-…..</span><br>
                                        5.4. <span> Selain itu Anda juga dapat membaca Frequently Asked Question (FAQ) yang telah kami siapkan untuk menjawab seputar pertanyaan-pertanyaan umum.</span>
                                    </ul>
                                </p>
                            </li>
                                
                            <li>
                                <strong>Jangka waktu Pelunasan</strong> 
                                <p class="kebijakan-ct">Jangka waktu pelunasan terhadap pembayaran yang dihasilkan kepada pemilik mobil  adalah sesuai kesepakatan dgn pembeli mobil dengan batas maksimal 3x24 jam.
                                </p>
                            </li>
                                                                
                        </ol>

                    </div>
                                                        </div>

                                                        <!-- <div class="d-none d-md-block">
                                                        <img s</ul>rc="images/img-banner.png" class="img-fluid img-banner" />
                    </div> -->
                </div>



            </div>
        </div>
        <!--<div class="row align-items-center">-->
          
        <!--    <div class="col-md-7 col-lg-8">-->
               
        <!--        <div class="row align-items-start">-->
                   
        <!--            <div class="col-lg-7 ">-->
        <!--                <ol>-->
        <!--                    <li>-->
        <!--                        Cek kisaran harga mobil Anda lalu jadwalkan waktu Anda untuk datang ke outlet kami-->
        <!--                    </li>-->
        <!--                    <li>-->
                             
        <!--                        Kami bantu cek kondisi mobil Anda dan memberikan harga terbaik untuk dimulainya penawaran-->
        <!--                    </li>-->
        <!--                    <li>-->
        <!--                        Pantau penawaran mobil Anda secara real time pada saat itu juga-->
        <!--                    </li>-->
        <!--                    <li>-->
        <!--                        Atur jadwal pembayaran mobil Anda dengan penawar tertinggi-->
        <!--                    </li>-->
                        
        <!--                </ol>-->

        <!--            </div>-->

        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    </section>
    <!-- <div class="show-banner">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" />
        </div>
    </div> -->


    <!-- <div class="show-banner center">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/test.jpg" class="img-fluid img-float" />
        </div>
    </div> -->
</main>

<?php
    $this->load->view('vfooter');
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    var app = angular.module('digih', []);
    
    app.controller('myCtrl', function($scope,$http) {
       var v1 = parent.document.URL.substring(parent.document.URL.indexOf('?'), parent.document.URL.length);
       var v2 = v1.replace("?", "")  
      var ah = v2.split("|");
        var mmax = ah[0]
        var mmin = ah[1]
     
        
    $http.get("http://hanggarmobil.com/apis/merk_.php").success(function (result_DataA) { 
        $scope.smerk=[];           
        $scope.smerk =  result_DataA['data'];
     //   console.log( $scope.smerk); 

    });
    $scope.find_model = function(val){
        $http.get("http://hanggarmobil.com/apis/model_.php",  {params:{"par": val}} ).success(function (result_DataA) { 
        $scope.smodel=[];           
        $scope.smodel =  result_DataA['data'];
     //   console.log( $scope.smerk); 
        });
    }
   
    
    
    });
        
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!--<script src="js/retina.min.js"></script>-->
<script src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lightcase.js"></script>
<script src="<?php echo base_url();?>assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script></body>
</html>