<footer class="container">
        <div class="row">
            <div class="col-md-4 d-md-none">
                <img src="<?php echo base_url();?>assets/images/img-banner.png" class="img-fluid" />
            </div>
            <div class="col-md-4 mb-3 mb-md-0">
            <h3>HangarMObil.com</h3>
                <p>HM merupakan perusahaan penyedia jasa penjualan mobil bekas dengan cara syariah.</p>
                <a href="<?php echo base_url("tentang");?>">< Profil selengkapnya</a>
            </div>
            <div class="col-md-4">
                <h3>Links</h3>
                   <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>kebijakan">Kebijakan Privasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>syarat">Syarat Ketentuan</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link active" href="<?php echo base_url();?>faq">FAQ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>lokasi">Lokasi Kami</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>kontak">Kontak</a>
                    </li>
                </ul>

            </div>
            <div class="col-md-4 d-none d-md-block">
                <img src="<?php echo base_url();?>assets/images/img-banner.png" class="img-fluid" />
            </div>
            <div class="col-md-4 mt-3">
                <small>© COPYRIGHT 2020  |  mobilkulaku -  PT. Artha Hutama Andalan</small>
            </div>
            <div class="col-md-4 mt-3">
                <div class="social-media-group">
                    <a href="#" class="social-media">
                        <i class="fa fa-facebook-f"></i>
                    </a>
                    <a href="#" class="social-media">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="#" class="social-media">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="social-media">
                        <i class="fa fa-youtube-play"></i>
                    </a>
                </div>
            </div>
        </div>
</footer>