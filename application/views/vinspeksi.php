<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html ng-app="digih" ng-controller="myCtrl"  lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

    <!--<link href="<?php echo base_url();?>assets/kal/bootstrap.min.css" rel="stylesheet" media="screen">-->
    <link href="<?php echo base_url();?>assets/kal/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


    <title>Hanggar Mobil</title>
</head>
<body>
 <!--<div ng-hide="hhide==1" class="loading">-->
 <!--     <div class="obj"></div>-->
 <!--     <div class="obj"></div>-->
 <!--     <div class="obj"></div>-->
 <!--     <div class="obj"></div>-->
 <!--     <div class="obj"></div>-->
 <!--     <div class="obj"></div>-->
 <!--     <div class="obj"></div>-->
 <!--     <div class="obj"></div>-->
 <!--   </div>-->
<header class="wrap-header">
    <!-- <div class="show-banner top">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" />
        </div>
    </div> -->
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand" href="#">
                <img src="<?php echo base_url();?>assets/images/logo.png" class="iamg-fluid" />
            </a>
            <button class="navbar-toggler hamburger hamburger--elastic" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMainContent" type="button" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
             <div class="collapse navbar-collapse" id="navbarMain">
                <div class="container">
                    <ul class="navbar-nav ml-auto justify-content-lg-end">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url();?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kerjakami">Cara Kerja Kami</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="<?php echo base_url();?>tentang">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>lokasi">Lokasi Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>kontak">Kontak</a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    </div>

</header>

<main>
    <section class="section section-banner" style="background-image:none;">
        <div class="container position-relative">
            <div class="row justify-content-end">
            <div class="col-md-7 col-lg-8">
                    <div class="row">
                        <div class="col-lg-7">
                            <h1>
                               Perkiraan harga mobil kamu adalah:
                            </h1>
                            <!-- <h1>
                                    Estimation of your car price:
                                </h1> -->

                                <!-- <h1>{{ textLoadingInspeksi }}</h1> -->
                            
                            <h1 > 
                               {{nmobil}}
                            </h1>
                            
                            <center style="margin-top: 2rem">
                            <div ng-if="loadingHarga" class="lds-ellipsis">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            </center>

                            <h1>
                                {{nharga}}
                            </h1>
<!--                         
                            <p>Estimation of your car price:</p> -->
                        </div>
                    </div>

                    <!-- <div class="d-none d-md-block">
                        <img src="images/img-banner.png" class="img-fluid img-banner" />
                    </div> -->
                </div>

                <div class="col-md-5 col-lg-4">
                    <form>
                        <div class="form-group relative cz-select">
                            <select required ng-disabled="loadApi.kota" ng-change="find_area(parea);loadApi.lokasi = true" ng-options="x.location as x.location for x in sarea" ng-model="parea"  class="form-control" required>
                                <!-- <option disabled value="">Select your City</option> -->
                                <option disabled value="">Pilih Kota</option>
                            </select>
                            <div class="loader" ng-if="!parea">
                                <i class="material-icons" ng-if="!loadApi.kota">keyboard_arrow_down</i>
                                <div class="loading-indicator" ng-if="loadApi.kota"></div>
                            </div>
                            <div class="loader" ng-if="parea"><i class="material-icons success">check</i></div>
                        </div>
                        <div class="form-group relative cz-select">
                            <select required ng-disabled="loadApi.lokasi || loadApi.lokasi == null" ng-options="x.alamat as x.alamat for x in ssekitar" ng-model="psekitar" class="form-control" required>
                                <option disabled value="">Pilih Lokasi di Sekitarmu</option>
                                <!-- <option disabled value="">Select a Location near you</option> -->

                            </select>
                            <div class="loader" ng-if="!psekitar">
                                <i class="material-icons" ng-if="!loadApi.lokasi">keyboard_arrow_down</i>
                                <div class="loading-indicator" ng-if="loadApi.lokasi"></div>
                            </div>
                            <div class="loader" ng-if="psekitar"><i class="material-icons success">check</i></div>
                        </div>
                        <!-- <div class="form-group"> -->
                                <!--<select ng-change="find_tahun(pmerk,pmodel)" ng-options="x.model as x.model for x in smodel" ng-model="pwaktu"  class="form-control" >-->
                                <!--      <option disabled value="">Pilih waktu perjanjian kamu</option>-->
                                    

                                <!--  </select>-->
                                
                        <div class="form-group relative cz-select" >
                            <input type="datetime" ng-click="toggleModalDateTime()" class="form-control" readonly 
                                placeholder="Pilih waktu perjanjian kamu " name="" id="dtp_input1"
                                ng-model="inputDateTime">
                            <div class="loader" ng-if="inputDateTime"><i class="material-icons success">check</i></div>
                            <!--<span  class="form-control form_datetime" style="color:black;">Pilih waktu perjanjian kamu</span>-->
                            <!-- <div style="margin-bottom: -20px;margin-left: -15px;" class="input-group date form_datetime col-md-11" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input placeholder="Pilih waktu perjanjian kamu" class="form-control" size="16" type="text" value="" readonly required>
                            
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input1"  /><br/> -->
                        </div>
                            
                            

                        <div class="form-group relative cz-select">
                            <select  class="form-control" required>
                                <option  ng-model="ppemilik" disabled selected>Apakah kamu pemilik kendaraan ini</option>
                                    <option value="ya">Ya</option>
                                    <option value="tidak">Tidak</option>
                                <!-- <option disabled value="">Are you the owner of this car?</option> -->

                            </select>
                            <div class="loader">
                            <i class="material-icons">keyboard_arrow_down</i>
                            </div>
                        </div>
                        <div class="form-group relative cz-select">
                            <select  class="form-control" required>
                                <option ng-model="pdarim" disabled selected>Dari mana kamu mengetahui kami?</option>
                                <option value="ig">Instagram</option>
                                    <option value="fb">Facebook</option>
                                    <option value="twitter">Twitter</option>
                                <!-- <option disabled value="">How did you hear about us?</option> -->
                            </select>
                            <div class="loader">
                            <i class="material-icons">keyboard_arrow_down</i>
                            </div>
                        </div>
                    
                        <!-- <button ng-click="find_harga(pmerk,pmodel,ptahun,ptmodel,ptransmisi,parea,pnama,pemail,pnohp)" class="btn btn-green">Confirm Booking</button> -->
                        <button ng-click="konfirm(parea,psekitar,pwaktu,ppemilik,pdarim,max_price,min_price)" class="btn btn-green">Konfirmasi Pemesanan</button>
                    </form>
                    <div class="d-md-none">
                        <img src="<?php echo base_url();?>assets/images/img-banner.png" class="img-fluid img-banner" />
                    </div>
                </div>
                

              


            </div>
        </div>
    </section>

    <!-- <section>
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-7 col-lg-8">
                    <h2>Kenapa harus di-<span>Hanggar?</span></h2>
                    <div class="row">
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="images/why-1.png" class="img-fluid mb-2" />
                            <h3>Terpercaya</h3>
                            <p class="small">Menjunjung tinggi kaidah<br/> penjualan yang sehat</p>
                        </div>
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="images/why-2.png" class="img-fluid mb-2" />
                            <h3>Transparansi</h3>
                            <p class="small">Proses lelang dapat <br/> dipantau bersama</p>
                        </div>
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="images/why-3.png" class="img-fluid mb-2" />
                            <h3>Mudah</h3>
                            <p class="small">Tidak pakai ribet <br/> dengan urusan perizinan</p>
                        </div>
                        <div class="col-sm-6 col-lg-3 text-center">
                            <img src="images/why-4.png" class="img-fluid mb-2" />
                            <h3>Maksimal</h3>
                            <p class="small">Harga tender akan <br/> dicapai sampai maksimal</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section class="bg-grey container">
        <div class="row align-items-center">
            <!-- <div class="col-md-5 col-lg-4">
                <div class="owl-carousel mobil-carousel">
                    <div class="item">
                        <img src="images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="images/test.jpg" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="images/test.jpg" class="img-fluid" />
                    </div>
                </div>
            </div> -->
            <div class="col-md-7 col-lg-8">
                <!--<h2>Blablabla</h2>-->
                <div class="row align-items-start">
                    <!-- <div class="col-lg-5">
                        <img src="images/loop.png" class="img-fluid mb-2" />
                    </div> -->
                    <!-- <div class="col-lg-7 ">
                        <ol>
                            <li>
                                <strong>Daftarkan mobil kamu melalui website</strong>
                                Lengkapi informasi mobil kamu dan sesuaikan kondisinya, selesaikan sampai konfirmasi
                            </li>
                            <li>
                                <strong>Daftarkan mobil kamu melalui website</strong>
                                Lengkapi informasi mobil kamu dan sesuaikan kondisinya, selesaikan sampai konfirmasi
                            </li>
                            <li>
                                <strong>Daftarkan mobil kamu melalui website</strong>
                                Lengkapi informasi mobil kamu dan sesuaikan kondisinya, selesaikan sampai konfirmasi
                            </li>
                            <li>
                                <strong>Daftarkan mobil kamu melalui website</strong>
                                Lengkapi informasi mobil kamu dan sesuaikan kondisinya, selesaikan sampai konfirmasi
                            </li>
                            <li>
                                <strong>Daftarkan mobil kamu melalui website</strong>
                                Lengkapi informasi mobil kamu dan sesuaikan kondisinya, selesaikan sampai konfirmasi
                            </li>
                        </ol>

                    </div> -->

                </div>
            </div>
        </div>
    </section>
    <!-- <div class="show-banner">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/footer-banner.jpg" class="img-fluid img-float" />
        </div>
    </div> -->

<!-- 
    <div class="show-banner center">
        <div class="wrapper">
            <button type="button" class="closeButton">
                <img src="images/x.png" class="img-fluid img-close" />
            </button>
            <img src="images/test.jpg" class="img-fluid img-float" />
        </div>
    </div> -->

    <div id="modal-datetime" ng-if="showModalDateTime">
        <div class="bg-modal-dark"></div>
        <div class="modal-dt-container animated bounceInDown">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="modal-dt-title">Pilih waktu perjanjian kamu</h1>

                    <i ng-click="toggleModalDateTime()" class="material-icons modal-dt-close">close</i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="modal-dt-left">
                        <ul class="list-group">
                            <li ng-class="{'modal-dt-item-active' : activeDate == item.date}" 
                                class="list-group-item modal-dt-item" ng-click="changeDTSelect(item)" 
                                ng-repeat="item in listDateTime">{{item.title}}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="modal-dt-right">
                    <div class="modal-dt-list-time" ng-repeat="tm in  listTime" 
                        ng-click="changeDTTime(tm)">{{tm.title}}:{{tm.minute}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<?php
    $this->load->view('vfooter');
?>

<script type="text/javascript" src="<?php echo base_url();?>assets/kal/jquery-1.8.3.min.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/kal/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/kal/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">
    $('.form_datetime').click(r => {
        console.log(r);
    })

</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/angular.min.js"></script>

<script type="text/javascript">
    var app = angular.module('digih', []);
 
    app.controller('myCtrl', function($scope,$http) {
        $scope.activeDate = new Date().getDate();
        $scope.dateTimeSelect = (item)=> {
            let date = new Date().getDate();
            if (item.date == date) {
                return true;
            } return false
        }
        $scope.getListDateTime = () => {
            var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
            var tgl = new Date();
            var today = new Date();
            var hari = tgl.getDate();
            var bulan = tgl.getMonth();
            var hariKe1 = today.getDay();
            var tgl2 = tgl.setDate(tgl.getDate() + 1);
            var hariKe2 = new Date(tgl2);
            var tgl3 = tgl.setDate(tgl.getDate() + 1);
            var hariKe3 = new Date(tgl3);
            var tgl4 = tgl.setDate(tgl.getDate() + 1);
            var hariKe4 = new Date(tgl4);
            var tgl5 = tgl.setDate(tgl.getDate() + 1);
            var hariKe5 = new Date(tgl5);
            var tgl6 = tgl.setDate(tgl.getDate() + 1);
            var hariKe6 = new Date(tgl6);
            var tgl7 = tgl.setDate(tgl.getDate() + 1);
            var hariKe7 = new Date(tgl7);
            var tgl8 = tgl.setDate(tgl.getDate() + 1);
            var hariKe8 = new Date(tgl8);
            
            // console.log(myDays[hariKe1], myDays[hariKe2], myDays[hariKe3], myDays[hariKe4])

            const dateTime = [
                {
                    title: `${myDays[hariKe1]}, ${today.getDate()} ${months[today.getMonth()]} `,
                    value: `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`,
                    date: today.getDate(),
                },
                {
                    title: `${myDays[hariKe2.getDay()]}, ${hariKe2.getDate()} ${months[hariKe2.getMonth()]} `,
                    value: `${hariKe2.getFullYear()}-${hariKe2.getMonth() + 1}-${hariKe2.getDate()}`,
                    date: hariKe2.getDate(),
                },
                {
                    title: `${myDays[hariKe3.getDay()]}, ${hariKe3.getDate()} ${months[hariKe3.getMonth()]} `,
                    value: `${hariKe3.getFullYear()}-${hariKe3.getMonth() + 1}-${hariKe3.getDate()}`,
                    date: hariKe3.getDate(),
                },
                {
                    title: `${myDays[hariKe4.getDay()]}, ${hariKe4.getDate()} ${months[hariKe4.getMonth()]} `,
                    value: `${hariKe4.getFullYear()}-${hariKe4.getMonth() + 1}-${hariKe4.getDate()}`,
                    date: hariKe4.getDate(),
                },
                {
                    title: `${myDays[hariKe5.getDay()]}, ${hariKe5.getDate()} ${months[hariKe5.getMonth()]} `,
                    value: `${hariKe5.getFullYear()}-${hariKe5.getMonth() + 1}-${hariKe5.getDate()}`,
                    date: hariKe5.getDate(),
                },
                {
                    title: `${myDays[hariKe6.getDay()]}, ${hariKe6.getDate()} ${months[hariKe6.getMonth()]} `,
                    value: `${hariKe6.getFullYear()}-${hariKe6.getMonth() + 1}-${hariKe6.getDate()}`,
                    date: hariKe6.getDate(),
                },
                {
                    title: `${myDays[hariKe7.getDay()]}, ${hariKe7.getDate()} ${months[hariKe7.getMonth()]} `,
                    value: `${hariKe7.getFullYear()}-${hariKe7.getMonth() + 1}-${hariKe7.getDate()}`,
                    date: hariKe7.getDate(),
                },
                {
                    title: `${myDays[hariKe8.getDay()]}, ${hariKe8.getDate()} ${months[hariKe8.getMonth()]} `,
                    value: `${hariKe8.getFullYear()}-${hariKe8.getMonth() + 1}-${hariKe8.getDate()}`,
                    date: hariKe8.getDate(),
                },
            ]

            return dateTime
        }
        $scope.changeDTSelect = (item) => {
            $scope.activeDate = item.date;
        }
        
        $scope.listDateTime = $scope.getListDateTime();
        
        $scope.listTime = [
            {   title: '07',hour: 07, minute: '00',},
            {   title: '08',hour: 08, minute: '00',},
            {   title: '09',hour: 09, minute: '00',},
            {   title: '10',hour: 10, minute: '00',},
            {   title: '11',hour: 11, minute: '00',},
            {   title: '12',hour: 12, minute: '00',},
            {   title: '13',hour: 13, minute: '00',},
            {   title: '14',hour: 14, minute: '00',},
            {   title: '15',hour: 15, minute: '00',},
            {   title: '16',hour: 16, minute: '00',}
        ]
        $scope.inputDateTime;

        // hasil dari dialog date time
        $scope.inputDateToSave;
        //

        $scope.changeDTTime = (time) => {
            let currentDate = new Date()
            let dateToSave = new Date(currentDate.getFullYear(), currentDate.getMonth(), $scope.activeDate, time.hour);
            $scope.inputDateToSave = dateToSave;
            console.log($scope.inputDateToSave)
            $scope.inputDateTime = `${dateToSave.getFullYear()}-${dateToSave.getMonth() + 1}-${dateToSave.getDate()} ${time.hour}:${time.minute}`;
            $scope.showModalDateTime = false;
        }

        $scope.showModalDateTime = false;
        $scope.toggleModalDateTime = () => {
            $scope.showModalDateTime = !$scope.showModalDateTime;
        }



        $scope.loadApi = {
            kota: true,
            lokasi: null
        }
        

        var url = 'http://localhost/hanggar_apis/';
    // var url = 'http://hanggarmobil.com/apis/';

        $scope.alert = (r) => {
            console.log(r);
        }
        $scope.fkal = function(){
            $scope.kal = 1
        }
    //  var namas = "<?=$pmerk;?>";
    //  console.log(namas)
    //   var v1 = parent.document.URL.substring(parent.document.URL.indexOf('?'), parent.document.URL.length);
    //   var v2 = v1.replace("?", "")  
    //     var ah = v2.split("|");
    //     var mmax = ah[0]
    //     var mmin = ah[1]
    //     var mmer = ah[2]
    //     var mmod = ah[3]
    //     var mtah = ah[4]
    $scope.loadingHarga = true;
    $http.get(url + "harga_.php",{
        params:{
            "pmerk": "<?=$pmerk;?>",
            "pmodel": "<?=$pmodel;?>",
            "ptahun": "<?=$ptahun;?>",
            "ptmodel": "<?=$ptmodel;?>",
            "ptransmisi": "<?=$ptransmisi;?>",
            "parea": "<?=$parea;?>"
        }
        }).success(function (result_DataA) { 
            $scope.loadingHarga = false;
        
        var formatter = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'IDR',
        });
        $scope.max_price =  result_DataA['data'][0]['max_price'];
        $scope.min_price =  result_DataA['data'][0]['min_price'];
        mmax = formatter.format($scope.max_price)
        mmin = formatter.format($scope.min_price)
        $scope.nharga = ""+mmin+" - "+mmax
        });
        
        $scope.nmobil = "<?=$pmerk;?>"+" "+"<?=$pmodel;?>"+" "+"<?=$ptahun;?>"
      
        
    $http.get(url + "merk_.php").success(function (result_DataA) { 
        $scope.smerk=[];           
        $scope.smerk =  result_DataA['data'];
     //   console.log( $scope.smerk); 

    });
    $scope.find_model = function(val){
        $http.get(url + "model_.php",  {params:{"par": val}} ).success(function (result_DataA) { 
        $scope.smodel=[];           
        $scope.smodel =  result_DataA['data'];
     //   console.log( $scope.smerk); 
        });
    }
    
    //  $scope.find_area = function(){
        $http.get(url + "area_.php").success(function (result_DataA) { 
        $scope.sarea=[];           
        $scope.sarea =  result_DataA['data'];

        $scope.loadApi.kota = false;
     //   console.log( $scope.smerk); 
        });
   // }
   
   
    $scope.find_area = function(val){
        $http.get(url + "sekitar_.php",  {params:{"par": val}} ).success(function (result_DataA) { 
        $scope.ssekitar=[];           
        $scope.ssekitar =  result_DataA['data'];
        
        $scope.loadApi.lokasi = false;
     //   console.log( $scope.smerk); 
        });
    }
   
    $scope.konfirm = function(parea,psekitar,pwaktu,ppemilik,pdarim,max_price,min_price){
        var x = max_price;
        var n = min_price;
        var tangl = document.getElementById("dtp_input1").value;
     //   var aa = element.innerHTML;

        console.log(tangl)
     //console.log(element)
      if(parea!=undefined && tangl!='' ){
        $http.get(url + "di_.php").success(function (result_DataA) { 
         
        var di =  result_DataA['data'][0]['counter'];
      
          $http.get(url + "konfirmasi_.php",{params:{"pmerk": "<?=$pmerk;?>","pmodel": "<?=$pmodel;?>","pyear": "<?=$ptahun;?>","pemail": "<?=$pemail;?>","pbookdate": tangl,"pbooktime": tangl,"pname": "<?=$pnama;?>","pphone": "<?=$pnohp;?>","ptransmission": "<?=$ptransmisi;?>","pmin_price": n,"pmax_price": x,"pid_booking": di,"pwilayah": parea}} ).success(function (result_DataA) { 
        $scope.hhasil=[];           
        $scope.hhasil =  result_DataA['message'];
    
         window.location.href = "<?php echo base_url();?>konfirmasi"
  
       
       
       
        });
        });
      
       
      }
       
    }
   
    
    
    });
        
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!--<script src="js/retina.min.js"></script>-->
<script src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lightcase.js"></script>
<script src="<?php echo base_url();?>assets/js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script></body>
</html>