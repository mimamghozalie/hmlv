<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inspeksi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	   //   $data = $_GET['p'];
	   //   var_dump(urldecode($data));
	   //   exit;
	    $data['pmerk'] = $_POST['pmerk'];
	    $data['pmodel'] = $_POST['pmodel'];
	    $data['ptahun'] = $_POST['ptahun'];
	    $data['ptmodel'] = $_POST['ptmodel'];
	    $data['ptransmisi'] = $_POST['ptransmisi'];
	    $data['parea'] = $_POST['parea'];
	    $data['pnama'] = $_POST['pnama'];
	    $data['pemail'] = $_POST['pemail'];
	    $data['pnohp'] = $_POST['pnohp'];

	  // $postData = $this->input->post();
	   //var_dump();
	   //exit;
		 $this->load->view('vinspeksi',$data);
	}
}
